
// Constant describing number of units menu buttons will have as a
// border from the view edge.
#define kMenuWindowBorder (10)

// Default names
#define kPlayer1DefaultName (@"P1")
#define kPlayer2DefaultName (@"P2")

// Textures for player characters - assumed to be circles
#define kPlayer1Sprite (@"blue_circle.png")
#define kPlayer2Sprite (@"green_circle.png")

// Textures for player base pieces
#define kPlayer1BasePiece (@"blue_base_triangle.png")
#define kPlayer2BasePiece (@"green_base_square.png")

// Joystick radius
#define kJoystickRadius (20)

// Texture for player base circles
#define kPlayerBaseSprite (@"grey_circle.png")

// Texture for text background
#define kTextBackgroundSprite (@"text_background.png")

// Distance from player bounding box to edge of screen on respawn
#define kPlayerEdgeBorder (15)
#define kBasePieceEdgeBorder (10)
#define kPauseEdgeBorder (30)

// Textures for obstacles - assumed to be circles
#define kObstacleRedSprite (@"red_circle.png")
#define kObstacleOrangeSprite (@"orange_circle.png")
#define kObstaclePurpleSprite (@"purple_circle.png")

// Number of obstacles in a game at one time
#define kNumberOfObstacles (10)

// Speed of obstacles and players
#define kObstacleSpeed (60)

// Player movement characteristics
#define kPlayerMass (6) // > 0
#define kPlayerMaxSpeed (250) // > 0
#define kPlayerDrag (90) // Speed retardation per second, [0, 100]

// Initial countdown timer value
#define kInitialCountDown (4)

// zOrders
#define kPlayerBaseZOrder (0)
#define kPlayerBasePieceZOrder (5)
#define kPlayerZOrder (10)
#define kObstacleZOrder (kPlayerZOrder)
#define kUIElementZOrder (100)

// UI Text element scaling factor
#define kUIElementScaling (3.0)