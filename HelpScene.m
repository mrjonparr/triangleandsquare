#import "HelpScene.h"
#import "MainScene.h"
#import "Config.h"

#import "cocos2d.h"

@implementation HelpScene

- (id) init
{
    if ((self = [super init]))
    {
        // We require touch here for taking us back to the main scene (in touchBegan).
        [self setUserInteractionEnabled:YES];

        CCButton *red_obstacle = [CCButton buttonWithTitle:@"red_obstacle" spriteFrame:[CCSpriteFrame frameWithImageNamed:kObstacleRedSprite]];

        // position is currently arbitrary.
        red_obstacle.position = ccp([[CCDirector sharedDirector] viewSize].width/2,[[CCDirector sharedDirector] viewSize].height/2);
        red_obstacle.block = ^(id sender)
        {
            NSLog(@"Red Obstacle was pressed.");
        };

        [self addChild:red_obstacle];
    }

    return self;
}

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    // Any touch on the help scene itself brings us straight back to the main scene.
    [[CCDirector sharedDirector] replaceScene:[MainScene node]];
}

@end