#import "Player.h"
#import "Config.h"

@implementation Player
{
    CGFloat x_speed;
    CGFloat y_speed;
}

+(id)create :(NSString*)image
{
    Player *p = [Player spriteWithImageNamed:image];
    p->x_speed = 0.0;
    p->y_speed = 0.0;
    p.zOrder = kPlayerZOrder;
    return p;
}

-(void)update :(CCTime)delta :(SneakyJoystick*)joystick
{
    // Calculate accelerations
    CGFloat x_accel = joystick.velocity.x * 10000.0 / kPlayerMass;
    CGFloat y_accel = joystick.velocity.y * 10000.0 / kPlayerMass;
    
    // Calculate new speeds
    assert((kPlayerDrag >= 0.0) && (kPlayerDrag <= 100.0));
    x_speed = (x_speed + x_accel * delta) * (100.0 - kPlayerDrag * delta) / 100.0;
    y_speed = (y_speed + y_accel * delta) * (100.0 - kPlayerDrag * delta) / 100.0;
    
    // Clamp speeds to max
    if (x_speed > kPlayerMaxSpeed)
        x_speed = kPlayerMaxSpeed;
    if (x_speed < -kPlayerMaxSpeed)
        x_speed = -kPlayerMaxSpeed;
    
    if (y_speed > kPlayerMaxSpeed)
        y_speed = kPlayerMaxSpeed;
    if (y_speed < -kPlayerMaxSpeed)
        y_speed = -kPlayerMaxSpeed;
    
    self.position = ccp(self.position.x + x_speed * delta, self.position.y + y_speed * delta);
}

@end