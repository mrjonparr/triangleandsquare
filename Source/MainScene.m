#import "MainScene.h"
#import "HelpScene.h"
#import "Config.h"
#import "Player.h"
#import "Obstacle.h"
#import "SneakyJoystick.h"
#import "Base.h"

#import "cocos2d.h"

@implementation PauseButton
{
    CCSprite *top;
    CCSprite *bot;
}

+(id)create
{
    PauseButton *b = [[PauseButton alloc] init];
    return b;
}

- (void)ready
{
    if (top.visible)
        bot.visible = TRUE;
    else
        top.visible = TRUE;
}
- (void)reset
{
    top.visible = FALSE;
    bot.visible = FALSE;
}

-(id)init
{
    if ((self = [super init]))
    {
        self.userInteractionEnabled = YES;
        
        // Add player 1 base pieces
        // Bottom left
        top = [CCSprite spriteWithImageNamed:@"pausebutton-top.png"];
        top.positionType = CCPositionTypeNormalized;
        top.position = CGPointMake(0.5, 0.5);
        top.anchorPoint = CGPointMake(0.5, 0.5);
        top.visible = NO;
        top.scale = 0.5f;
        [self addChild:top];
        
        // Bottom right
        bot = [CCSprite spriteWithImageNamed:@"pausebutton-bottom.png"];
        bot.positionType = CCPositionTypeNormalized;
        bot.position = CGPointMake(0.5, 0.5);
        bot.anchorPoint = CGPointMake(0.5, 0.5);
        bot.visible = NO;
        bot.scale = 0.5f;
        [self addChild:bot];
   
        self.contentSize = [bot contentSize];
        self.anchorPoint = CGPointMake(0.5, 0.5);
    }
    
    return self;
}

-(void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    NSLog(@"Touched pause button");
    [[CCDirector sharedDirector] replaceScene:[MainScene node]];
}

@end

@implementation MainScene
{
    CCTextField *enterName_P1;
    CCButton *p1_name;
    CCTextField *enterName_P2;
    CCButton *p2_name;

    CCButton *help;

    int base_ready_count;

    int countdown_timer;
    CCLabelTTF *countdown_text;
    
    Player *p1;
    Player *p2;
    
    TriangleBase *triangleBase;
    SquareBase *squareBase;
    
    SneakyJoystick *p1_joystick;
    SneakyJoystick *p2_joystick;
    
    NSMutableArray *obstacles;
    NSMutableArray *bases;
    
    PauseButton *pause_button;
}

- (void)onCountdownTick: (id) sender
{
    countdown_timer--;

    countdown_text.string = [NSString stringWithFormat:@"%i", countdown_timer];
    
    switch (countdown_timer)
    {
        case 3:
            // Take bases back to their original positions ready for starting the game.
            [triangleBase reset];
            [squareBase reset];
            // Show the countdown text
            countdown_text.visible = TRUE;
            [self spawnObstacles];
            // Enable pause button (but only the top half)
            [pause_button ready];
            break;
        case 2:
            [pause_button ready];
            break;
        case 1:
            break;
        case 0:
            [self unschedule:@selector(onCountdownTick:)];
            [self startGame];
            
            [self removeChild:countdown_text];
            break;
        default:
            break;
    }
}

- (void)enteredName: (id) sender
{
    if (sender == enterName_P1)
    {
        NSString *input = [enterName_P1.string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([input isEqualToString:@""])
            p1_name.label.string = kPlayer1DefaultName;
        else
            p1_name.label.string = enterName_P1.string;
        enterName_P1.visible = FALSE;
        p1_name.anchorPoint = CGPointMake(0, 0);
        [p1_name unscheduleAllSelectors];
    }
    else if (sender == enterName_P2)
    {
        NSString *input = [enterName_P2.string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([input isEqualToString:@""])
            p2_name.label.string = kPlayer2DefaultName;
        else
            p2_name.label.string = enterName_P2.string;
        enterName_P2.visible = FALSE;
        [p2_name unscheduleAllSelectors];
    }
    else
    {
        assert(!"Unknown sender!");
    }
}

- (void)playerReady: (id) player_base
{
    if (player_base == squareBase)
    {
        p2_name.enabled = FALSE;
        p2_name.label.opacity = 0.5f;
        base_ready_count += 1;
    }
    else if (player_base == triangleBase)
    {
        p1_name.enabled = FALSE;
        p1_name.label.opacity = 0.5f;
        base_ready_count += 1;
    }
    else
    {
        assert(!"Unknown player base");
    }
    
    if (base_ready_count == 2)
    {
        base_ready_count = 0;
        
        // Disable help button
        help.visible = NO;
        help.enabled = NO;
        
        [self schedule:@selector(onCountdownTick:) interval:1.0f];
    }
}

- (void)chooseName: (id) player_name
{
    if (player_name == p1_name && !enterName_P2.visible)
    {
        enterName_P1.visible = !enterName_P1.visible;
    }
    else if (player_name == p2_name && !enterName_P1.visible)
    {
        enterName_P2.visible = !enterName_P2.visible;
    }
}

- (void) spawnObstacles
{
    // Add obstacles
    obstacles = [NSMutableArray arrayWithCapacity:kNumberOfObstacles];
    for (int i = 0; i < kNumberOfObstacles; ++i)
    {
        Obstacle *o = [Obstacle create];
        
        [self addChild:o];
        [obstacles addObject:o];
    }
}

- (void) startGame
{
    NSLog(@"Launching game scene ...");
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    
    // Add player 1
    p1 = [Player create:kPlayer1Sprite];
    p1.position = ccp(viewSize.width/2, p1.boundingBox.size.height / 2 + kPlayerEdgeBorder);
    [self addChild:p1];

    // Ensure player 1 is the same size as an obstacle
    Obstacle *o = obstacles[0];
    assert(o.boundingBox.size.width == p1.boundingBox.size.width);
    assert(o.boundingBox.size.height == p1.boundingBox.size.height);
    
    // Add player 2
    p2 = [Player create:kPlayer2Sprite];
    p2.position = ccp(viewSize.width/2, viewSize.height - p2.boundingBox.size.height / 2 - kPlayerEdgeBorder);
    [self addChild:p2];
    
    // Ensure the players are the same size
    assert(p1.boundingBox.size.width == p2.boundingBox.size.width);
    assert(p1.boundingBox.size.height == p2.boundingBox.size.height);
    
    // Add player 1's joystick
    p1_joystick = [[SneakyJoystick alloc] initWithRect:CGRectMake(0.0f, 0.0f, viewSize.width, viewSize.height/2)];
    [p1_joystick setJoystickRadius:kJoystickRadius];
    [self addChild:p1_joystick];
    
    // Add player 2's joystick
    p2_joystick = [[SneakyJoystick alloc] initWithRect:CGRectMake(0.0f, viewSize.height/2, viewSize.width, viewSize.height/2)];
    [p2_joystick setJoystickRadius:kJoystickRadius];
    [self addChild:p2_joystick];
}

- (id) init
{
    if ((self = [super init]))
    {
        CGSize viewSize = [[CCDirector sharedDirector] viewSize];
        
        base_ready_count = 0;
        countdown_timer = kInitialCountDown;

        countdown_text = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", countdown_timer] fontName:@"Monaco" fontSize:46];
        countdown_text.position = ccp(viewSize.width/2,viewSize.height/2);
        [self addChild:countdown_text];
        countdown_text.visible = FALSE;

        p1_name = [CCButton buttonWithTitle:kPlayer1DefaultName];
        p1_name.anchorPoint = ccp(0, 0);
        p1_name.position = ccp(kMenuWindowBorder, kMenuWindowBorder);
        p1_name.zOrder = kUIElementZOrder;
        p1_name.scale = kUIElementScaling;
        [self addChild:p1_name];

        p2_name = [CCButton buttonWithTitle:kPlayer2DefaultName];
        p2_name.anchorPoint = ccp(0, 0);
        p2_name.rotation = 180.0f;
        p2_name.position = ccp(viewSize.width - kMenuWindowBorder, viewSize.height - kMenuWindowBorder);
        p2_name.zOrder = kUIElementZOrder;
        p2_name.scale = kUIElementScaling;
        [self addChild:p2_name];

        help = [CCButton buttonWithTitle:@"?"];
        help.anchorPoint = ccp(1.0, 0.5);
        help.position = ccp(viewSize.width - kMenuWindowBorder, viewSize.height/2);
        help.zOrder = kUIElementZOrder;
        help.scale = kUIElementScaling;
        help.block = ^(id sender)
        {
            [[CCDirector sharedDirector] replaceScene:[HelpScene node]];
        };
        [self addChild:help];

        enterName_P1 = [CCTextField textFieldWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:kTextBackgroundSprite]];
        enterName_P1.fontSize = 16.0f;
        enterName_P1.contentSize = CGSizeMake(100.0f, 50.0f);
        enterName_P1.preferredSize =CGSizeMake(100.0f, 50.0f);
        enterName_P1.anchorPoint = ccp(0,0);
        enterName_P1.position = ccp(viewSize.width / 2 - (enterName_P1.boundingBox.size.width/2), viewSize.height/2 - (enterName_P1.boundingBox.size.height/2));
        enterName_P1.visible = FALSE;
        [self addChild:enterName_P1 z:5];
        
        enterName_P2 = [CCTextField textFieldWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:kTextBackgroundSprite]];
        enterName_P2.fontSize = 16.0f;
        enterName_P2.contentSize = CGSizeMake(100.0f, 50.0f);
        enterName_P2.preferredSize =CGSizeMake(100.0f, 50.0f);
        enterName_P2.anchorPoint = ccp(0,0);
        enterName_P2.position = ccp(viewSize.width / 2 - (enterName_P2.boundingBox.size.width/2), viewSize.height/2 - (enterName_P2.boundingBox.size.height/2));
        enterName_P2.visible = FALSE;
        [self addChild:enterName_P2 z:5];

        [p1_name setTarget:self selector:@selector(chooseName:)];
        [p2_name setTarget:self selector:@selector(chooseName:)];

        [enterName_P1 setTarget:self selector:@selector(enteredName:)];
        [enterName_P2 setTarget:self selector:@selector(enteredName:)];
        
        // Add player bases spheres
        CCSprite *p1Base = [CCSprite spriteWithImageNamed:kPlayerBaseSprite];
        p1Base.position = ccp(viewSize.width / 2, 0);
        p1Base.zOrder = kPlayerBaseZOrder;
        [self addChild:p1Base];
        CCSprite *p2Base = [CCSprite spriteWithImageNamed:kPlayerBaseSprite];
        p2Base.position = ccp(viewSize.width / 2, viewSize.height);
        p2Base.zOrder = kPlayerBaseZOrder;
        [self addChild:p2Base];

        // Add player bases
        triangleBase = [TriangleBase create:self];
        triangleBase.zOrder = kPlayerBasePieceZOrder;
        [self addChild:triangleBase];
        
        squareBase = [SquareBase create:self];
        squareBase.zOrder = kPlayerBasePieceZOrder;
        [self addChild:squareBase];
        
        pause_button = [PauseButton create];
        pause_button.zOrder = kUIElementZOrder;
        [pause_button setPosition:ccp(kPauseEdgeBorder,viewSize.height/2)];
        [self addChild:pause_button];
    }
 
    return self;
}

-(void)update:(CCTime)delta
{
    [p1 update:delta :p1_joystick];
    [p2 update:delta :p2_joystick];
}

@end