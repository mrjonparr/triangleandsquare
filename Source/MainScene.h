@interface MainScene : CCScene

- (void)chooseName: (id) sender;
- (void)enteredName: (id) sender;
- (void)playerReady: (id) sender;

@end

@interface PauseButton : CCNode

- (void)ready;
- (void)reset;

@end