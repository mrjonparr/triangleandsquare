//
//  joystick.m
//  SneakyJoystick
//
//  Created by Nick Pannuto.
//  2/15/09 verion 0.1
//  
//  WIKI: http://wiki.github.com/sneakyness/SneakyJoystick/
//  HTTP SRC: http://github.com/sneakyness/SneakyJoystick.git
//  GIT: git://github.com/sneakyness/SneakyJoystick.git
//  Email: SneakyJoystick@Sneakyness.com 
//  IRC: #cocos2d-iphone irc.freenode.net

#import "SneakyJoystick.h"

#define SJ_PI 3.14159265359f
#define SJ_PI_X_2 6.28318530718f
#define SJ_RAD2DEG 180.0f/SJ_PI
#define SJ_DEG2RAD SJ_PI/180.0f

@interface SneakyJoystick(hidden)
- (void)updateVelocity:(CGPoint)point;
- (void)setTouchRadius;
@end

@implementation SneakyJoystick

@synthesize
stickPosition,
degrees,
velocity,
area,
center,
autoCenter,
isDPad,
hasDeadzone,
numberOfDirections,
joystickRadius,
thumbRadius,
deadRadius;


-(id)initWithRect:(CGRect)rect
{
	self = [super init];
	if (self)
    {
		stickPosition = CGPointZero;
		degrees = 0.0f;
		velocity = CGPointZero;
        area = rect;
        center = CGPointZero;
		autoCenter = YES;
		isDPad = NO;
		hasDeadzone = NO;
		numberOfDirections = 4;
		
		self.joystickRadius = fmin(CGRectGetWidth(rect)/2, CGRectGetHeight(rect)/2);
		self.thumbRadius = 32.0f;
		self.deadRadius = 0.0f;
        
        [self setUserInteractionEnabled:YES];
        [self setMultipleTouchEnabled:NO];
		
		//Cocos node stuff
        self.anchorPoint = ccp(0.0f, 0.0f);
        self.contentSize = rect.size;
		self.position = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    }
	return self;
}

-(void)updateVelocity:(CGPoint)point
{
	// Calculate distance and angle from the center.
	float dx = point.x;
	float dy = point.y;
	float dSq = dx * dx + dy * dy;
	
	if(dSq <= deadRadiusSq){
		velocity = CGPointZero;
		degrees = 0.0f;
		stickPosition = point;
		return;
	}

	float angle = atan2f(dy, dx); // in radians
	if(angle < 0){
		angle		+= SJ_PI_X_2;
	}
	float cosAngle;
	float sinAngle;
	
	if(isDPad){
		float anglePerSector = 360.0f / numberOfDirections * SJ_DEG2RAD;
		angle = roundf(angle/anglePerSector) * anglePerSector;
	}
	
	cosAngle = cosf(angle);
	sinAngle = sinf(angle);
	
	// NOTE: Velocity goes from -1.0 to 1.0.
	if (dSq > joystickRadiusSq || isDPad) {
		dx = cosAngle * joystickRadius;
		dy = sinAngle * joystickRadius;
	}
	
	velocity = CGPointMake(dx/joystickRadius, dy/joystickRadius);
	degrees = angle * SJ_RAD2DEG;
	
	// Update the thumb's position
	stickPosition = ccp(dx, dy);
}

- (void) setIsDPad:(BOOL)b
{
	isDPad = b;
	if(isDPad){
		hasDeadzone = YES;
		self.deadRadius = 10.0f;
	}
}

- (void) setJoystickRadius:(float)r
{
	joystickRadius = r;
	joystickRadiusSq = r*r;
}

- (void) setThumbRadius:(float)r
{
	thumbRadius = r;
	thumbRadiusSq = r*r;
}

- (void) setDeadRadius:(float)r
{
	deadRadius = r;
	deadRadiusSq = r*r;
}

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
    
    // Before we convert to our node space, check we're in our area
    if (!CGRectContainsPoint(area, location))
        return;
    
    // Now convert to our node space
	location = [self convertToNodeSpace:location];
    
    // Set the new center of the joystick
    center = location;
    
    // No speed yet
    [self updateVelocity:CGPointZero];
}

- (void)touchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
	location = [self convertToNodeSpace:location];
    
    location = CGPointMake(location.x - center.x, location.y - center.y);
	[self updateVelocity:location];
}

- (void)touchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint location = CGPointZero;
	if(!autoCenter){
		location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
		location = [self convertToNodeSpace:location];
        location = CGPointMake(location.x - center.x, location.y - center.y);
	}
	[self updateVelocity:location];
}

- (void)touchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self touchEnded:touch withEvent:event];
}

@end
