#import "SneakyJoystick.h"

@interface Player : CCSprite

+(id)create :(NSString*)image;

-(void)update :(CCTime)delta :(SneakyJoystick*)joystick;

@end
