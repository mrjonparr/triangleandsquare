#import "Base.h"
#import "Config.h"
#import "MainScene.h"

@implementation Base
{
    MainScene *notifier_scene;
}

-(id)initWithScene:(MainScene*)scene
{
    if ((self = [super init]))
    {
        notifier_scene = scene;
    }
    
    return self;
}

-(void)ready
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    self.position = ccp(self.position.x, viewSize.height / 2);
    self.userInteractionEnabled = NO;
    [notifier_scene playerReady:self];
}

@end

@implementation TriangleBase
{
    CCSprite *pieces[4];
}

+(id)create:(MainScene*)notifier_scene
{
    TriangleBase *b = [[TriangleBase alloc] initWithScene:notifier_scene];
    return b;
}

-(void)reset
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    self.position = ccp(viewSize.width / 2, pieces[0].boundingBox.size.height + kBasePieceEdgeBorder);
}

-(id)initWithScene:(MainScene*)scene
{
    if ((self = [super initWithScene:scene]))
    {
        self.userInteractionEnabled = YES;
        
        // Add player 1 base pieces
        // Bottom left
        pieces[0] = [CCSprite spriteWithImageNamed:kPlayer1BasePiece];
        pieces[0].positionType = CCPositionTypeNormalized;
        pieces[0].position = CGPointMake(0.5, 0.5);
        pieces[0].anchorPoint = CGPointMake(1, 1);
        [self addChild:pieces[0]];
        
        // Bottom right
        pieces[1] = [CCSprite spriteWithImageNamed:kPlayer1BasePiece];
        pieces[1].positionType = CCPositionTypeNormalized;
        pieces[1].position = CGPointMake(0.5, 0.5);
        pieces[1].anchorPoint = CGPointMake(0, 1);
        [self addChild:pieces[1]];
        
        // Top
        pieces[2] = [CCSprite spriteWithImageNamed:kPlayer1BasePiece];
        pieces[2].positionType = CCPositionTypeNormalized;
        pieces[2].position = CGPointMake(0.5, 0.5);
        pieces[2].anchorPoint = CGPointMake(0.5, 0);
        [self addChild:pieces[2]];
        
        // Middle
        pieces[3] = [CCSprite spriteWithImageNamed:kPlayer1BasePiece];
        pieces[3].positionType = CCPositionTypeNormalized;
        pieces[3].position = CGPointMake(0.5, 0.5);
        pieces[3].anchorPoint = CGPointMake(0.5, 1);
        pieces[3].flipY = YES;
        [self addChild:pieces[3]];

        self.contentSize = CGSizeMake(pieces[0].contentSize.width * 2, pieces[0].contentSize.height * 2);
        self.anchorPoint = CGPointMake(0.5, 0.5);
        [self reset];

    }
    
    return self;
}

-(void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    NSLog(@"Touched p1 base");
    [self ready];
}

@end

@implementation SquareBase
{
    CCSprite *pieces[4];
}

+(id)create:(MainScene*)notifier_scene
{
    SquareBase *b = [[SquareBase alloc] initWithScene:notifier_scene];
    return b;
}

-(void)reset
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    self.position = ccp(viewSize.width / 2, viewSize.height - pieces[0].boundingBox.size.height - kBasePieceEdgeBorder);
}

-(id)initWithScene:(MainScene*)scene
{
    if ((self = [super initWithScene:scene]))
    {
        self.userInteractionEnabled = YES;

        // Add player 2 base pieces
        // Top left
        pieces[0] = [CCSprite spriteWithImageNamed:kPlayer2BasePiece];
        pieces[0].positionType = CCPositionTypeNormalized;
        pieces[0].position = CGPointMake(0.5, 0.5);
        pieces[0].anchorPoint = CGPointMake(1, 0);
        [self addChild:pieces[0]];
        
        // Top right
        pieces[1] = [CCSprite spriteWithImageNamed:kPlayer2BasePiece];
        pieces[1].positionType = CCPositionTypeNormalized;
        pieces[1].position = CGPointMake(0.5, 0.5);
        pieces[1].anchorPoint = CGPointMake(0, 0);
        [self addChild:pieces[1]];
        
        // Bottom left
        pieces[2] = [CCSprite spriteWithImageNamed:kPlayer2BasePiece];
        pieces[2].positionType = CCPositionTypeNormalized;
        pieces[2].position = CGPointMake(0.5, 0.5);
        pieces[2].anchorPoint = CGPointMake(1, 1);
        [self addChild:pieces[2]];
        
        // Bottom right
        pieces[3] = [CCSprite spriteWithImageNamed:kPlayer2BasePiece];
        pieces[3].positionType = CCPositionTypeNormalized;
        pieces[3].position = CGPointMake(0.5, 0.5);
        pieces[3].anchorPoint = CGPointMake(0, 1);
        [self addChild:pieces[3]];

        self.contentSize = CGSizeMake(pieces[0].contentSize.width * 2, pieces[0].contentSize.height * 2);
        self.anchorPoint = CGPointMake(0.5, 0.5);
        [self reset];
    }
    
    return self;
}

-(void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    NSLog(@"Touched p2 base");
    [self ready];
}

@end