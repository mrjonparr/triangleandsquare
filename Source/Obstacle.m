#import "Obstacle.h"
#import "Config.h"

@implementation Obstacle
{
    CGFloat x_speed;
    CGFloat y_speed;
}

-(void) initPosition
{
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    
    // Find edge for new obstacle, 0 left, 1 top, 2 right, 3 bottom
    int edge = arc4random_uniform(4);
    
    // Find how far along the edge, in %. 0 furthest right facing inward, 100 furthest left
    CGFloat distance_along_edge = arc4random_uniform(100);
    CGFloat distance_along_width = (viewSize.width - self.boundingBox.size.width) * distance_along_edge / 100.0 + (self.boundingBox.size.width / 2);
    int distance_along_height = (viewSize.height - self.boundingBox.size.height) * distance_along_edge / 100.0 + (self.boundingBox.size.height / 2);
    
    // Find direction of travel of new obstacle, in degrees. 0 is right facing inwards, 180 is left
    int direction = arc4random_uniform(89) + 45;
    
    // Set obstacle starting position
    CGFloat x, y;
    switch (edge % 2)
    {
        case 0: // left or right
            y = distance_along_height;
            switch (edge / 2)
        {
            case 0: // left
                x = -self.boundingBox.size.width / 2;
                direction = 270 + direction;
                break;
            case 1: // right
                x = viewSize.width + self.boundingBox.size.width / 2;
                direction = 90 + direction;
                break;
        }
            break;
        case 1: // top or bottom
            x = distance_along_width;
            switch (edge / 2)
        {
            case 0: // top
                y = viewSize.height + self.boundingBox.size.width / 2;
                direction = 180 + direction;
                break;
            case 1: // bottom
                y = -self.boundingBox.size.width / 2;
                // direction unchanged
                break;
        }
            break;
    }
    [self setPosition:ccp(x, y)];
    self->x_speed = cos(direction * M_PI / 180.0);
    self->y_speed = sin(direction * M_PI / 180.0);
}

+(id) create
{
    Obstacle *o;
    switch (arc4random_uniform(3))
    {
        case 0:
            o = [Obstacle spriteWithImageNamed:kObstacleRedSprite];
            break;
        case 1:
            o = [Obstacle spriteWithImageNamed:kObstacleOrangeSprite];
            break;
        case 2:
            o = [Obstacle spriteWithImageNamed:kObstaclePurpleSprite];
            break;
    }

    [o initPosition];
    o.zOrder = kObstacleZOrder;
    return o;
}

-(void) reset
{
    switch (arc4random_uniform(3))
    {
        case 0:
            [self setTexture:[[CCSprite spriteWithImageNamed:kObstacleRedSprite] texture]];
            break;
        case 1:
            [self setTexture:[[CCSprite spriteWithImageNamed:kObstacleOrangeSprite] texture]];
            break;
        case 2:
            [self setTexture:[[CCSprite spriteWithImageNamed:kObstaclePurpleSprite] texture]];
            break;
    }

    [self initPosition];
}

-(void) update:(CCTime)delta
{
    self.position = ccp(self.position.x + kObstacleSpeed * delta * x_speed, self.position.y + kObstacleSpeed * delta * y_speed);
    
    // Reset obstacle if it has moved out of the play area
    CGSize viewSize = [[CCDirector sharedDirector] viewSize];
    if ((self.position.x < -self.boundingBox.size.width / 2) ||
        (self.position.x > viewSize.width + self.boundingBox.size.width / 2) ||
        (self.position.y < -self.boundingBox.size.height / 2) ||
        (self.position.y > viewSize.height + self.boundingBox.size.height / 2))
    {
        [self reset];
    }
}

@end