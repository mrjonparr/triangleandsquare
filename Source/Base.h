#import "MainScene.h"

@interface Base : CCNode

-(void)ready;

@end

@interface TriangleBase : Base

+(id)create:(MainScene*)scene;

-(void)reset;

@end

@interface SquareBase : Base

+(id)create:(MainScene*)scene;

-(void)reset;

@end
